//
//  ExpandAnimator.swift
//  TransitionSpike
//
//  Created by Ronan Clancy on 08/07/2016.
//  Copyright © 2016 Ronan Clancy. All rights reserved.
//

import UIKit

class ExpandAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    var screenStatusBarHeight: CGFloat {
        return UIApplication.sharedApplication().statusBarFrame.height
    }
    
    let duration = 0.5
    var originFrame = CGRect.zero
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return duration
    }
    
    func animateTransition(context: UIViewControllerContextTransitioning) {
        if let peopleVC = context.viewControllerForKey(UITransitionContextFromViewControllerKey) as? PeopleViewController,
            cvVC = context.viewControllerForKey(UITransitionContextToViewControllerKey) as? CVViewController {
            moveFromPeople(peopleVC, cvVC: cvVC, withContext: context)
        }
        else if let cvVC = context.viewControllerForKey(UITransitionContextToViewControllerKey) as? PeopleViewController,
            peopleVC = context.viewControllerForKey(UITransitionContextFromViewControllerKey) as? CVViewController {
            moveFromCV(peopleVC, toPeople: cvVC, withContext: context)
        }
    }
    
    func moveFromPeople(peopleVC: PeopleViewController, cvVC: CVViewController, withContext context: UIViewControllerContextTransitioning) {
    }
    
    func moveFromCV(cvVC: CVViewController, toPeople peopleVC: PeopleViewController, withContext context: UIViewControllerContextTransitioning) {
    }
    
    private func createTransitionImageViewWithFrame(frame: CGRect, peopleViewController: PeopleViewController, cvVC: CVViewController) -> UIImageView {
        let imageView = UIImageView(frame: frame)
        imageView.frame.origin.y = imageView.frame.origin.y + (peopleViewController.navigationController!.navigationBar.frame.size.height + screenStatusBarHeight) - cvVC.mainScrollView.contentOffset.y
        imageView.contentMode = .ScaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }
}

