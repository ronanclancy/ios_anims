//
//  UIUtils.swift
//  TransitionSpike
//
//  Created by Ronan Clancy on 07/07/2016.
//  Copyright © 2016 Ronan Clancy. All rights reserved.
//

import UIKit

extension  UIView {
    
    /// Function to add a shadow to TableView cell cell
    func addShadow() {
        self.layer.shadowOffset = CGSizeMake(0, 2)
        self.layer.shadowColor = UIColor.lightGrayColor().CGColor
        self.layer.shadowRadius = 0.3
        self.layer.shadowOpacity = 0.7
        self.layer.cornerRadius = 1
        self.layer.masksToBounds = false;
        self.clipsToBounds = false;
        
        let shadowFrame: CGRect = (self.layer.bounds)
        let shadowPath: CGPathRef = UIBezierPath(rect: shadowFrame).CGPath
        self.layer.shadowPath = shadowPath
    }
}
