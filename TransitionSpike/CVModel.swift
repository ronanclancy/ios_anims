//
//  CVModel.swift
//  TransitionSpike
//
//  Created by Ronan Clancy on 26/06/2016.
//  Copyright © 2016 Ronan Clancy. All rights reserved.
//

import Foundation

class CVModel {

    var name: String
    var age: Int
    var tagLine: String
    var avatar: String
    
    init(name: String, age: Int, tagLine: String, avatar: String) {
        self.name = name
        self.age = age
        self.tagLine = tagLine
        self.avatar = avatar
    }
}
