//
//  Animator.swift
//  TransitionSpike
//
//  Created by Ronan Clancy on 23/06/2016.
//  Copyright © 2016 Ronan Clancy. All rights reserved.
//

import UIKit

class CollectionViewCellAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    var screenStatusBarHeight: CGFloat {
        return UIApplication.sharedApplication().statusBarFrame.height
    }
    
    let duration = 0.5
    var originFrame = CGRect.zero
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return duration
    }
    
    func animateTransition(context: UIViewControllerContextTransitioning) {
        if let peopleVC = context.viewControllerForKey(UITransitionContextFromViewControllerKey) as? PeopleViewController,
            cvVC = context.viewControllerForKey(UITransitionContextToViewControllerKey) as? CVViewController {
            moveFromPeople(peopleVC, cvVC: cvVC, withContext: context)
        }
        else if let cvVC = context.viewControllerForKey(UITransitionContextToViewControllerKey) as? PeopleViewController,
            peopleVC = context.viewControllerForKey(UITransitionContextFromViewControllerKey) as? CVViewController {
            moveFromCV(peopleVC, toPeople: cvVC, withContext: context)
        }
    }
    
    func moveFromPeople(peopleVC: PeopleViewController, cvVC: CVViewController, withContext context: UIViewControllerContextTransitioning) {
        
        //1) setup the transition
        context.containerView()!.addSubview(cvVC.view)
        let containerView = context.containerView()!
        
        //2) Setup scale effect.
        let finalFrame = cvVC.mainImage.frame
        let xScaleFactor = originFrame.width / finalFrame.width
        let yScaleFactor = originFrame.height / finalFrame.height
        let scaleTransform = CGAffineTransformMakeScale(xScaleFactor, yScaleFactor)
        
        // 3) Here we set the view state of the presented controller elements during transition
        cvVC.detailsTitle.alpha = 0.0
        cvVC.detailsTableView.alpha = 0.0
        cvVC.tagLineLable.alpha = 0.0
        cvVC.aboutLabel.alpha = 0.0
        
        cvVC.mainImage.transform = scaleTransform
        cvVC.mainImage.center = CGPoint(
            x: CGRectGetMidX(originFrame),
            y: CGRectGetMidY(originFrame) - (cvVC.navigationController!.navigationBar.frame.size.height + screenStatusBarHeight)
        )
        cvVC.mainImage.contentMode = .ScaleAspectFill
        
        containerView.addSubview((cvVC.view)!)        
        
        // Animate main cvImage image
        UIView.animateWithDuration(duration, delay: 0.0, options: [], animations: {
            cvVC.mainImage.transform = CGAffineTransformIdentity
            cvVC.mainImage.center = CGPoint( x: CGRectGetMidX(finalFrame), y: CGRectGetMidY(finalFrame))
            }, completion: nil)
        
        // Animate cvName
        UIView.animateWithDuration(1.0, delay: duration, usingSpringWithDamping: 0.50, initialSpringVelocity: 0.7, options: .CurveEaseInOut, animations: {
            cvVC.detailsTitle.alpha = 1.0
            cvVC.detailsTitle.center.y -= 50
            }, completion: nil)
        
        // Animate tagLine
        UIView.animateWithDuration(duration, delay: duration, options: .CurveEaseInOut, animations: {
            cvVC.tagLineLable.center.y -= 70
            cvVC.tagLineLable.alpha = 1.0
            cvVC.detailsTableView.alpha = 1.0
            }, completion: nil)
        
        UIView.animateWithDuration(duration, delay: 0.8, options: .CurveEaseInOut, animations: {
            cvVC.aboutLabel.alpha = 1.0
            }, completion: {_ in
                context.completeTransition(!context.transitionWasCancelled())
        })
    }
    
    func moveFromCV(cvVC: CVViewController, toPeople peopleVC: PeopleViewController, withContext context: UIViewControllerContextTransitioning) {
        
        //1) setup the transition
        context.containerView()!.insertSubview(peopleVC.view, belowSubview: cvVC.view)
        peopleVC.view.alpha = 0.0
        peopleVC.itemsCollectionView.alpha = 0.0
        cvVC.mainImage.alpha = 0.0
        
        //2) animations!
        let imageView = createTransitionImageViewWithFrame(cvVC.mainImage.frame, peopleViewController: peopleVC, cvVC: cvVC)
        imageView.image = cvVC.mainImage.image
        context.containerView()!.addSubview(imageView)
        
        UIView.animateWithDuration(0.4, animations: {
            cvVC.view.alpha = 0.0
            peopleVC.view.alpha = 1.0
            imageView.frame = self.originFrame
            peopleVC.itemsCollectionView.alpha = 1.0
        }) { finished in
            imageView.removeFromSuperview()
            context.completeTransition(!context.transitionWasCancelled())
        }
    }
    
    private func createTransitionImageViewWithFrame(frame: CGRect, peopleViewController: PeopleViewController, cvVC: CVViewController) -> UIImageView {
        let imageView = UIImageView(frame: frame)
        imageView.frame.origin.y = imageView.frame.origin.y + (peopleViewController.navigationController!.navigationBar.frame.size.height + screenStatusBarHeight) - cvVC.mainScrollView.contentOffset.y
        imageView.contentMode = .ScaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }
}
