//
//  DetailsViewController.swift
//  TransitionSpike
//
//  Created by Ronan Clancy on 23/06/2016.
//  Copyright © 2016 Ronan Clancy. All rights reserved.
//

import UIKit

class CVViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var detailsTitle: UILabel!
    @IBOutlet weak var detailsTableView: UITableView!
    @IBOutlet weak var tagLineLable: UILabel!
    @IBOutlet weak var aboutLabel: UITextView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var mainScrollView: UIScrollView!    
    @IBOutlet weak var mainImageTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var mainImageBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewContainer: UIView!
    @IBOutlet weak var cardViewHolder: UIView!
    
    var titleImageView: UIImageView?
    var titleTextView: UITextView?
    
    var cvModel: CVModel?
    var didAnimateTitle: Bool = false
    
    var hobbies = [ HobbyModel(name: "Programming", avatar: "h1", description: "dsflgkjsdf;klgjsdf;klgjs;ldkfjg;"),
                    HobbyModel(name: "Photography", avatar: "h2", description: "dsflgkjsdf;klgjsdf;klgjs;ldkfjg;"),
                    HobbyModel(name: "Astrology", avatar: "h3", description: "dsflgkjsdf;klgjsdf;klgjs;ldkfjg;"),
                    HobbyModel(name: "Programming", avatar: "h1", description: "dsflgkjsdf;klgjsdf;klgjs;ldkfjg;"),
                    HobbyModel(name: "Photography", avatar: "h2", description: "dsflgkjsdf;klgjsdf;klgjs;ldkfjg;"),
                    HobbyModel(name: "Astrology", avatar: "h3", description: "dsflgkjsdf;klgjsdf;klgjs;ldkfjg;")]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpNavbarTitle()
        
        // Data setup
        mainImage.image = UIImage(named: (cvModel?.avatar)!)
        detailsTitle.text = cvModel?.name
        tagLineLable.text = cvModel?.tagLine
        detailsTableView.separatorStyle = .None
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        /// Add title effect
        if mainScrollView.contentOffset.y >= 225 && !didAnimateTitle {
            self.titleTextView?.center.y = 40
            UIView.animateWithDuration(0.8, delay: 0.0, usingSpringWithDamping: 0.50, initialSpringVelocity: 0.7, options: .CurveEaseInOut, animations: {
                self.titleTextView?.alpha = 1.0
                self.titleTextView?.hidden = false
                self.titleTextView?.center.y = 20
                self.didAnimateTitle = true
                }, completion: nil)
        } else if mainScrollView.contentOffset.y < 225 && didAnimateTitle {
            UIView.animateWithDuration(0.5, delay: 0.0, options: .CurveEaseInOut, animations: {
                self.titleTextView?.alpha = 0.0
                self.didAnimateTitle = false
                }, completion:nil)
        }
        
        /// Add paralax effect to main brand image
        if mainScrollView.contentOffset.y >= 0 {
            imageViewContainer.clipsToBounds = true
            mainImageBottomConstraint?.constant = -mainScrollView.contentOffset.y / 2
            mainImageTopConstraint?.constant = mainScrollView.contentOffset.y / 2
        } else {
            mainImageTopConstraint?.constant = mainScrollView.contentOffset.y
            imageViewContainer.clipsToBounds = true
        }
        
        /// diable table view scroll until full screen scope
        if mainScrollView.contentOffset.y >= 330 {
            detailsTableView.scrollEnabled = true
        } else {
            detailsTableView.scrollEnabled = false
        }
    }
    
    func setUpNavbarTitle(){
        titleTextView = UITextView(frame: CGRectMake(0, 0, 0, 40))
        titleTextView!.hidden = true
        titleTextView!.backgroundColor = UIColor.clearColor()
        titleTextView!.text = cvModel?.name
        titleTextView?.font = UIFont.systemFontOfSize(22, weight: UIFontWeightThin)
        titleTextView?.sizeToFit()
        navigationItem.titleView = titleTextView
    }
}

/// hack to enable view hierarchy when debbuging :\
extension UITextView {
    func _firstBaselineOffsetFromTop() {
    }
    func _baselineOffsetFromBottom() {
    }
}


extension CVViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hobbies.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("hobbyCell") as! HobbyCustomCell
        cell.configure(UIImage(named: (hobbies[indexPath.row].avatar))!, hobbyTitle: hobbies[indexPath.row].name)
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // to implement
    }
}