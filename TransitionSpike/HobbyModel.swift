//
//  Hobbie.swift
//  TransitionSpike
//
//  Created by Ronan Clancy on 28/06/2016.
//  Copyright © 2016 Ronan Clancy. All rights reserved.
//

import Foundation

class HobbyModel {
    
    var name: String
    var avatar: String
    var decription: String
    
    init(name: String, avatar: String, description: String) {
        self.name = name
        self.avatar = avatar
        self.decription = description
    }
}

