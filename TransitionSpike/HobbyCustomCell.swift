//
//  File.swift
//  TransitionSpike
//
//  Created by Ronan Clancy on 28/06/2016.
//  Copyright © 2016 Ronan Clancy. All rights reserved.
//

import UIKit

class HobbyCustomCell: UITableViewCell {
    
    @IBOutlet weak var hobbyImage: UIImageView!
    @IBOutlet weak var hobbyTitle: UILabel!    
    @IBOutlet weak var hobbyDescription: UILabel!
    @IBOutlet weak var cardHolder: UIView!
    
    func configure(hobbyImage: UIImage, hobbyTitle: String){
        self.hobbyImage.image = hobbyImage
        self.hobbyTitle.text = hobbyTitle
        cardHolder.addShadow()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
}