//
//  ItemsCustomCell.swift
//  TransitionSpike
//
//  Created by Ronan Clancy on 23/06/2016.
//  Copyright © 2016 Ronan Clancy. All rights reserved.
//

import UIKit

class ItemsCustomCell: UICollectionViewCell {

    @IBOutlet weak var itemsImage: UIImageView!
    
    func configure(itemImage: UIImage){
        self.itemsImage.image = itemImage
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }

}
