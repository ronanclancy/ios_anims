//
//  ViewController.swift
//  TransitionSpike
//
//  Created by Ronan Clancy on 23/06/2016.
//  Copyright © 2016 Ronan Clancy. All rights reserved.
//

import UIKit

class PeopleViewController: UIViewController {
    
    let reuseIdentifier = "personCell"
    var people = [  CVModel(name: "Annonymous", age: 21, tagLine: "Because the light of day has no say!", avatar: "14"),
                    CVModel(name: "Shinead", age: 26, tagLine: "Because the light of day has no say!", avatar: "2"),
                    CVModel(name: "George", age: 44, tagLine: "Because the light of day has no say!", avatar: "3"),
                    CVModel(name: "Marry", age: 32, tagLine: "Because the light of day has no say!", avatar: "15"),
                    CVModel(name: "George", age: 45, tagLine: "Because the light of day has no say!", avatar: "5"),
                    CVModel(name: "Stefo", age: 35, tagLine: "Because the light of day has no say!", avatar: "6"),
                    CVModel(name: "Jack", age: 5, tagLine: "Because the light of day has no say!", avatar: "7"),
                    CVModel(name: "Trump", age: 4, tagLine: "Because the light of day has no say!", avatar: "8"),
                    CVModel(name: "John", age: 17, tagLine: "Because the light of day has no say!", avatar: "9"),
                    CVModel(name: "Gregg", age: 36, tagLine: "Because the light of day has no say!", avatar: "10"),
                    CVModel(name: "Syina", age: 19, tagLine: "Because the light of day has no say!", avatar: "11"),
                    CVModel(name: "Hulk", age: 45, tagLine: "Because the light of day has no say!", avatar: "12"),
                    CVModel(name: "Ronan", age: 105, tagLine: "Because the light of day has no say!", avatar: "13")]
    
    var selectedImage: CGRect?
    let animator = CollectionViewCellAnimator()
    
    @IBOutlet weak var itemsCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension PeopleViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.people.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! ItemsCustomCell
        cell.configure(UIImage(named: people[indexPath.row].avatar)!)
        return cell
    }
    
    func collectionView (collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
        let width = (collectionView.bounds.width/2) - 20;
        return CGSizeMake(width, width - 50);
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cellFrameInSuperview = collectionView.convertRect(collectionView.cellForItemAtIndexPath(indexPath)!.frame, toView: view)
        selectedImage = cellFrameInSuperview
        performSegueWithIdentifier("detailsSegue", sender: indexPath)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "detailsSegue"){
            let detailsVC = segue.destinationViewController as! CVViewController
            let indexPath = sender as! NSIndexPath
            detailsVC.cvModel = people[indexPath.item]
            self.navigationController?.delegate = self
        }
    }
}

extension PeopleViewController : UINavigationControllerDelegate {
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        animator.originFrame = selectedImage!
        return animator
    }
}